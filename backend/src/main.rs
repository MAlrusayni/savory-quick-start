use warp::Filter;

#[tokio::main]
async fn main() {
    // serve index.html file at any path
    let index = warp::any().and(warp::fs::file("index.html"));
    // serve our app dir, this dir will contain the wasm app file `savory.wasm`
    let app = warp::path("app").and(warp::fs::dir("app"));

    println!("[INFO] Starting server at http://localhost:8080");
    warp::serve(app.or(index))
        .run(([0, 0, 0, 0], 8080))
        .await;
}
