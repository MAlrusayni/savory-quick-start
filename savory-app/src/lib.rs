use savory::prelude::*;
use savory_elements::prelude::*;
use savory_style::{prelude::*, unit::px, values as val, Color, St};

// app element (the model)
pub struct Counter(i32);

// app message
pub enum Msg {
    Increment,
    Decrement,
}

impl Element for Counter {
    type Message = Msg;
    type Config = Url;

    // initialize the app in this function
    fn init(_: Url, _: &mut impl Orders<Msg>) -> Self {
        Self(0)
    }

    // handle app messages
    fn update(&mut self, msg: Msg, _: &mut impl Orders<Msg>) {
        match msg {
            Msg::Increment => self.0 += 1,
            Msg::Decrement => self.0 -= 1,
        }
    }
}

impl View<Node<Msg>> for Counter {
    // view the app
    fn view(&self) -> Node<Msg> {
        // sharde style for buttons
        let styler = |style: Style| {
            style
                .push(St::Appearance, val::None)
                .background(Color::SlateBlue)
                .text(Color::White)
                .and_border(|conf| conf.none().radius(px(4)))
                .margin(px(4))
                .padding(px(4))
        };

        Flex::row()
            .center()
            .push(
                html::button()
                    .and_style(styler)
                    .push("Increment")
                    .on_click(|_| Msg::Increment),
            )
            .push(&self.0.to_string())
            .push(
                html::button()
                    .and_style(styler)
                    .push("Decrement")
                    .on_click(|_| Msg::Decrement),
            )
            .view()
    }
}

#[wasm_bindgen(start)]
pub fn view() {
    // mount and start the app at `app` element
    Counter::start();
}
