#!/usr/bin/env bash

wasm-pack build --target web --out-dir ../backend/app --out-name savory-app --no-typescript
